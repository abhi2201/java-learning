package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {
	// write your code here
        for(int i=1;i<=100;i++)
        {
            System.out.println(i);
           if(i==50)
                continue; //continue loop with next iteration
            if(i==50)
                break; // loops terminate on specified condition

            System.out.println("end");
        }
    }
}
